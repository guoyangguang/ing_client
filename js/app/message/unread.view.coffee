define [
    'backbone',
    'cs!app/lib/composite.view',
    'underscore',
    'cs!app/lib/flash_message',
    'cs!app/message/betweens',
    'cs!app/message/betweens.view',
    'text!app/template/message/unread.html'
], (Backbone, CompositeView, _, FlashMessage,
    Betweens, BetweensView, unreadHtml)->

    class UnreadView extends CompositeView
    
        tagName: 'li'
        className: 'unread'
        
        initialize: (options)->
            @flashMsg = new FlashMessage()

        events: { 
           'click .message-link': 'fetchBetween'
        }

        fetchBetween: ->
            console.log @model.get('profile'), typeof @model.get('profile')
            betweens = new Betweens([], {theOther: @model.get('profile')['id']})
            betweensView = new BetweensView({
                collection: betweens
            })
            betweensView.fetch()
            @appendChild(betweensView)
            # @$el.animate({opacity: 1}, 'fast','linear')

        render: ->
            template = _.template(unreadHtml)
            message_body = @model.get('body')
            if message_body.length > 35 
                @model.set({'body':  message_body.slice(0, 35) + '...'})
            @$el.html template(@model.attributes)
            @
