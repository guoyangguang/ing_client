define ['backbone', 'cs!app/message/between'], (Backbone, Between)->
  
    class Betweens extends Backbone.Collection
    
        initialize: (models, options)->
            @page = 1
            @currentLen = 0
            @theOther = options.theOther if options.theOther

        model: Between 

        url: '/api/messages/between'
