define ['backbone', 'cs!app/message/contact'], (Backbone, Contact)->
  
    class Contacts extends Backbone.Collection
    
        initialize: ->
            @currentLen = 0
            
        model: Contact 

        url: '/api/messages/contacts/mine'
