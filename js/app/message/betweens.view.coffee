define [
    'cs!app/lib/composite.view',
    'underscore',
    'cs!app/message/between.view',
    'cs!app/csrf/csrf',
    'cs!app/lib/flash_message',
    'cs!app/lib/utils',
    'text!app/template/message/betweens.html'
], (CompositeView, _, BetweenView, CSRF, FlashMessage, Utils, betweensHtml)->
  
    class BetweensView extends CompositeView
      
        id: 'betweens'
        
        initialize: (options)->
            @$el.html(betweensHtml)
            @listenTo(@collection, 'add', @addCallback)
            _.bindAll(
                @, 'successFetch', 'errorFetch', 'successCreate',
                'errorCreate', 'successCSRF', 'errorCSRF'  
            )
            @flashMsg = new FlashMessage()

        events: {
            'click .betweens-more': 'fetch',
            'click .betweens-send': 'fetchCSRF',
            'click .betweens-close': 'close'
        }

        fetch: ->
            data = {
                access_token: window.localStorage.getItem('accessToken'), 
                the_other: @collection.theOther, 
                page: @collection.page
            } 
            @collection.fetch({
                data: data, 
                wait: true, 
                success: @successFetch, 
                error: @errorFetch,
                remove: false
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @, 'betweens-more', '.betweens-body')

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            @create(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMessage.remote(response)

        create: (csrfToken)->
            msg = @$('.betweens-input').val()
            data = {
                csrf_token: csrfToken,
                access_token: window.localStorage.getItem('accessToken'), 
                body: msg,
                recipient_id: @collection.theOther
            } 
            @collection.create(
                data, wait: true, success: @successCreate, error: @errorCreate
            )

        successCreate: (model, response, options)->
            @$('.betweens-input').val('')

        errorCreate: (model, response, options)->
            @flashMsg.remote(response)

        addCallback: (model)->
            betweenView = new BetweenView(model: model)
            @appendChildTo(betweenView, '.betweens')

        close: ->
            @leave()
