define [
    'cs!app/lib/composite.view',
    'underscore',
    'cs!app/message/betweens',
    'cs!app/message/betweens.view',
    'text!app/template/message/contact.html'
], (CompositeView, _, Betweens, BetweensView, contactHtml)->
  
    class ContactView extends CompositeView
    
        tagName: 'li'
        className: 'contact'
        
        events: {
            'click .contact-name a': 'fetchBetween'  
        }
        
        fetchBetween: ->
            betweens = new Betweens([], {theOther: @model.id})
            betweensView = new BetweensView({
                collection: betweens
            })
            betweensView.fetch()
            @appendChild(betweensView)
            # @$el.animate({opacity: 1}, 'fast','linear')

        render: ->
            template = _.template(contactHtml)
            @$el.html template(@model.attributes)
            @
