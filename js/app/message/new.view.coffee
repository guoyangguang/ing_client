define [
    'cs!app/lib/composite.view',
    'underscore',
    'cs!app/csrf/csrf',
    'cs!app/lib/flash_message',
    'text!app/template/message/new.html'
], (CompositeView, _, CSRF, FlashMessage, newHtml)->
  
    class NewMsgView extends CompositeView
      
        className: 'new-msg pure-form'
        
        initialize: (options)->
            @$el.html(newHtml)
            _.bindAll(
                @, 'successCSRF', 'errorCSRF', 'successSend', 'errorSend'
            )
            @flashMsg = new FlashMessage()
            @theOther = options.theOther if options.theOther

        events: {
            'click .new-msg-close': 'close',
            'click .new-msg-send': 'fetchCSRF'
        }

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            @send(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMsg.remote(response)

        send: (csrfToken)->
            if window.App.router.ifSignin()
                data = {
                    csrf_token: csrfToken,
                    access_token: window.localStorage.getItem('accessToken'), 
                    body: @$('.new-msg-input').val(),
                    recipient_id: @theOther
                } 
                @model.url = '/api/messages/between'
                @model.save(
                    data, wait: true, success: @successSend, error: @errorSend
                )

        successSend: (model, response, options)->
            @close()
            @flashMsg.local('已寄出.')

        errorSend: (model, response, options)->
            @flashMsg.remote(response)

        close: ->
            @parent.hasNewMsg = false
            @leave()
