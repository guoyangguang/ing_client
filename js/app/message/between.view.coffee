define [
    'cs!app/lib/composite.view',
    'underscore',
    'text!app/template/message/between.html'
], (CompositeView, _, betweenHtml)->

    class BetweenView extends CompositeView
    
        tagName: 'li'
        className: 'between'

        render: ->
            template = _.template(betweenHtml)
            @$el.html(template(@model.attributes))
            @
