define ['backbone', 'cs!app/message/unread'], (Backbone, Unread)->
  
    class Unreads extends Backbone.Collection
    
        initialize: ->
            @page = 1
            @currentLen = 0

        model: Unread 

        url: '/api/messages/unread/mine'
