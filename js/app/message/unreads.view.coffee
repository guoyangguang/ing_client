define [
    'cs!app/lib/composite.view',
    'underscore',
    'cs!app/message/unread.view',
    'cs!app/lib/flash_message',
    'cs!app/lib/utils',
    'text!app/template/message/unreads.html'
], (CompositeView, _, UnreadView, FlashMessage, Utils, unreadsHtml)->
  
    class UnreadsView extends CompositeView
    
        id: 'unreads'
        
        initialize: (options)->
            @$el.html(unreadsHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(@, 'successFetch', 'errorFetch')
            @listenTo(@collection, 'add', @addCallback)

        events: {
            'click .unreads-more': 'fetch'
        }

        fetch: ()->
            data = {
                access_token: window.localStorage.getItem('accessToken'), 
                page: @collection.page
            }
            @collection.fetch({
                data: data,
                wait: true, 
                success: @successFetch,
                error: @errorFetch,
                remove: false
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @, 'unreads-more')

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        addCallback: (model)->
            unreadView = new UnreadView({model: model})
            @appendChildTo(unreadView, '.unreads')
