define [
    'cs!app/lib/composite.view',
    'underscore',
    'cs!app/lib/flash_message',
    'cs!app/message/contact.view',
    'text!app/template/message/contacts.html'
], (CompositeView, _, FlashMessage, ContactView, contactsHtml)->
  
    class ContactsView extends CompositeView
    
        id: 'contacts'

        initialize: (options)->
            @$el.html(contactsHtml)
            @listenTo(@collection, 'add', @addCallback)
            _.bindAll(@, 'successFetch', 'errorFetch')
            @flashMsg = new FlashMessage()
        
        fetch: ->
            data = {
                access_token: window.localStorage.getItem('accessToken')
            } 
            @collection.fetch({
                data: data, 
                wait: true, 
                success: @successFetch, 
                error: @errorFetch,
                remove: false
            })
        
        successFetch: (collection, response, options)->

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)
          
        addCallback: (model)-> 
            @collection.currentLen++
            contactView = new ContactView(model: model)
            @appendChildTo(contactView, '.contacts')
            if @collection.currentLen > 1 and @collection.currentLen % 5 is 0
                contactView.$el.after('<br/>')
