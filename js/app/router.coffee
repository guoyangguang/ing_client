define [
  'backbone', 
  'jquery', 
  'cs!app/lib/flash_message',
  'cs!app/menu/menu.view',
  'cs!app/user/auth', 
  'cs!app/user/signin.view', 
  'cs!app/user/signout.view', 
  'cs!app/user/signup.view',
  'cs!app/user/reset_password.view',
  'cs!app/profile/profile',
  'cs!app/profile/profile.view',
  'cs!app/message/unreads',
  'cs!app/message/unreads.view',
  'cs!app/message/contacts',
  'cs!app/message/contacts.view',
  'cs!app/item/pubitems',
  'cs!app/item/pubitems.view',
  'cs!app/item/pubitems_kind',
  'cs!app/item/pubitems_kind.view',
  'cs!app/item/pubitems_search',
  'cs!app/item/pubitems_search.view',
  'cs!app/like/pubitems_mylikes',
  'cs!app/like/pubitems_mylikes.view',
  'cs!app/item/pubitem',
  'cs!app/item/pubitem_show.view',
  'cs!app/item/pubitems_her',
  'cs!app/item/pubitems_her.view',
  'cs!app/item/myitems',
  'cs!app/item/myitems.view',
  'cs!app/item/myitem',
  'cs!app/item/myitem_show.view',
  'cs!app/access_token/access_token',
  'cs!app/access_token/access_token.view',
  'cs!app/captcha/captcha',
  'cs!app/captcha/captcha.view'
], (Backbone, $, FlashMessage, MenuView,
    Auth, SigninView, SignoutView, SignupView,
    ResetPasswordView, Profile, ProfileView,
    Unreads, UnreadsView, Contacts, ContactsView, PubItems, PubItemsView,
    PubItemsKind, PubItemsKindView, PubItemsSearch, PubItemsSearchView,
    PubItemsMyLikes, PubItemsMyLikesView, PubItem, PubItemShowView,
    PubItemsHer, PubItemsHerView, Myitems, MyitemsView, MyItem, MyItemShowView, 
    AccessToken, AccessTokenView, Captcha, CaptchaView
   )->
    
    class Router extends Backbone.Router
    
        initialize: (options)->
            @menuView = new MenuView()
            @flashMessage = new FlashMessage()
            $('body').prepend(@menuView.el)

        swap: (newView=null)->  
            @currentView.leave() if @currentView && @currentView.leave
            @currentView = newView
        
        ifSignin: ->
            if window.localStorage.getItem('accessToken')
                true
            else
                @navigate('signin', {trigger: true})
                false
        
        routes: {
            'signup(/)': 'signup',
            'signin(/)': 'signin',
            'signout(/)': 'signout',
            'reset_password(/)': 'resetPassword',
            'profile(/)': 'profile',
            'messages/unread/mine(/)': 'fetchUnreadMsg',
            'messages/contacts/mine(/)': 'fetchContacts',
            '(/)': 'pubitems',
            'pubitems/kind/:kind/:city(/)': 'pubitemsKind',
            'pubitems/search/:query/:city(/)': 'pubitemsSearch',
            'pubitems/likes/mine(/)': 'pubitemsMyLike',
            'pubitems/:itemId(/)': 'pubitem_show',
            'items/mine(/)': 'myItems',
            ':site/oauth/callback(/)': 'accessToken'
        }

        signup: -> 
            if window.localStorage.getItem('accessToken')
                @flashMessage.local('请退出.')
                @navigate('', {trigger: true})
            else
                signup = new Auth()
                signupView = new SignupView(model: signup)
                @swap(signupView)
                captcha = new Captcha()
                captchaView = new CaptchaView({model: captcha})
                captchaView.fetchCaptcha()
                signupView.renderChildInto(captchaView, '.signup-captcha')
                $('#main').append(signupView.el)
        
        signin: ->
            if window.localStorage.getItem('accessToken')
                @navigate('', {trigger: true})
            else
                signin = new Auth()
                signinView = new SigninView(model: signin)
                @swap(signinView)
                captcha = new Captcha()
                captchaView = new CaptchaView({model: captcha})
                captchaView.fetchCaptcha()
                signinView.renderChildInto(captchaView, '.signin-captcha')
                $('#main').append(signinView.el)
       
        signout: -> 
            if window.localStorage.getItem('accessToken')
                @menuView.swapMenu()
                auth = new Auth()
                signoutView = new SignoutView(model: auth)
                signoutView.fetchCSRF()
            else
                @navigate('signin', {trigger: true})

        resetPassword: ->
            if window.localStorage.getItem('accessToken')
                @flashMessage.local('请退出.')
                @navigate('', {trigger: true})
            else
                resetPass = new Auth()
                resetPassView = new ResetPasswordView(
                    model: resetPass
                )
                @swap(resetPassView)
                captcha = new Captcha()
                captchaView = new CaptchaView({model: captcha})
                captchaView.fetchCaptcha()
                resetPassView.renderChildInto(captchaView, '.reset-password-captcha')
                $('#main').append(resetPassView.el)

        profile: ->
            if @ifSignin()
                @menuView.swapMenu('#menu-me')
                profile = new Profile()
                profileView = new ProfileView({model: profile})
                profileView.fetch()
                @swap(profileView)
                $('#main').append(profileView.el)

        fetchUnreadMsg: ->
            if @ifSignin()
                @menuView.swapMenu('#menu-msg')
                unreads = new Unreads()
                unreadsView = new UnreadsView({collection: unreads})
                unreadsView.fetch()
                @swap(unreadsView)
                $('#main').append(unreadsView.el)

        fetchContacts: ->
            if @ifSignin()
                @menuView.swapMenu('#menu-msg')
                contacts = new Contacts()
                contactsView = new ContactsView({collection: contacts})
                contactsView.fetch()
                @swap(contactsView)
                $('#main').append(contactsView.el)

        pubitems: ->
            if @ifSignin()
                @menuView.swapMenu()
                pubitems = new PubItems()
                pubitemsView = new PubItemsView({collection: pubitems})
                pubitemsView.fetch()
                @swap(pubitemsView) 
                $('#main').append(pubitemsView.el)

        pubitemsKind: (kind, city)->
            if @ifSignin()
                @menuView.swapMenu()
                items = new PubItemsKind()
                itemsView = new PubItemsKindView({
                    collection: items, kind: kind, city: city 
                })
                itemsView.fetch()
                @swap(itemsView) 
                $('#main').append(itemsView.el)

        pubitemsSearch: (query, city)->
            if @ifSignin()
                @menuView.swapMenu()
                items = new PubItemsSearch()
                itemsView = new PubItemsSearchView({
                    collection: items, query: query, city: city
                })
                itemsView.fetch()
                @swap(itemsView) 
                $('#main').append(itemsView.el)

        pubitemsMyLike: ->
            if @ifSignin()
                @menuView.swapMenu('#menu-me')
                pubitemsMyLikes = new PubItemsMyLikes()
                pubitemsMyLikesView = new PubItemsMyLikesView(
                    {collection: pubitemsMyLikes}
                )
                pubitemsMyLikesView.fetch()
                @swap(pubitemsMyLikesView) 
                $('#main').append(pubitemsMyLikesView.el)

        pubitem_show: (itemId, parentView=null)->
            # there are two ways to show pubitem
            if @ifSignin()
                pubitem = new PubItem()
                pubitemShowView = new PubItemShowView({model: pubitem})
                pubitemShowView.fetch(itemId)
                if parentView
                    parentView.appendChild(pubitemShowView)
                else
                    $('#main').append(pubitemShowView.el)

        pubitems_her: (profileId, parentView)->
            if @ifSignin()
                pubitemsHer = new PubItemsHer()
                pubitemsHerView = new PubItemsHerView({
                    collection: pubitemsHer,
                    profileId: profileId
                })
                pubitemsHerView.fetch()
                parentView.appendChild(pubitemsHerView)

        myItems: ->
            if @ifSignin()
                @menuView.swapMenu('#menu-me')
                myitems = new Myitems()
                myitemsView = new MyitemsView({collection: myitems})
                myitemsView.fetch()
                @swap(myitemsView) 
                $('#main').append(myitemsView.el)

        myitem_show: (itemId, parentView)->
            if @ifSignin()
                myitem = new MyItem()
                myitemShowView = new MyItemShowView({model: myitem})
                myitemShowView.fetch(itemId)
                parentView.appendChild(myitemShowView)

        accessToken: (site, queryParam)->
            if @ifSignin()
                if queryParam.match(/code=/)
                    code = queryParam.replace(/code=/, '')
                    accessToken = new AccessToken()
                    accessTokenView = new AccessTokenView(
                        {model: accessToken, site: site, code: code}
                    )
                    accessTokenView.fetchCSRF()
                    $('#main').append(accessTokenView.el)
                else
                    @flashMessage.local('未授权.')
