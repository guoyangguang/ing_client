define [
    'cs!app/lib/composite.view',
    'underscore',
    'cs!app/lib/flash_message',
    'text!app/template/captcha/captcha.html'
], (CompositeView, _, FlashMessage, captchaHtml)->


    class CaptchaView extends CompositeView

        tagName: 'p'
        className: 'captcha'

        initialize: (options)->
            _.bindAll(@, 'successFetch', 'errorFetch')
            @listenTo(@model, 'change', @changeCallback)
            @flashMessage = new FlashMessage()

        fetchCaptcha: ->
            @model.fetch(
                {wait: true, success: @successFetch, error: @errorFetch}
            )

        successFetch: (model, response, options)->

        errorFetch: (model, response, options)->
            @flashMessage.remote(response)

        changeCallback: ->
            @$el.html(
                _.template(captchaHtml)(@model.attributes)
            )
