define ['backbone'], (Backbone)->
    
    class CSRF extends Backbone.Model

        url: '/api/csrf_token'
