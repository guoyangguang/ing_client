define [
    'cs!app/lib/composite.view',
    'cs!app/access_token/access_token',
    'cs!app/access_token/share.view',
    'text!app/template/access_token/share_btn.html'
], (CompositeView, AccessToken, ShareView, shareBtnHtml)-> 


    class ShareBtnView extends CompositeView

        className: 'share-btn' 

        initialize: (options)->
            @itemStatus = options.itemStatus
            @itemId = options.itemId
            @$el.html(shareBtnHtml)
            @hasShare = false

        events: {
            'click .share-btn-btn': 'appendShare'
        }

        appendShare: ->
            if not @hasShare
                access_token = new AccessToken()
                shareView = new ShareView({model: access_token})
                @appendChild(shareView)
                @hasShare = true
