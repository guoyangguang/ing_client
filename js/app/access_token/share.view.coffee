define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/lib/flash_message',
    'text!app/template/access_token/share.html'
], (_, CompositeView, FlashMessage, shareHtml)-> 


    class ShareView extends CompositeView

        className: 'share pure-form'

        initialize: (options)->
            @$el.html(shareHtml)
            _.bindAll(@, 'successFetch', 'errorFetch')
            @flashMsg = new FlashMessage()

        events: {
            'click .share-close': 'close',
            'click .share-sub': 'fetch',
            'mouseover .share-site': 'showAuth',
            'mouseleave .share-site': 'hideAuth'
        }

        close: ->
            @leave()
            @parent.hasShare = false

        fetch: ->
            if window.App.router.ifSignin()
                # NOTE only pubitem is shared
                if @parent.itemStatus is 2
                    shareLink = "http://127.0.0.1/pubitems/#{@parent.itemId}" 
                    url = '/api/access_tokens/mine/pubstatus?'
                    sitesArr = []
                    if @$('.share-site-douban:checked').length is 1 
                        sitesArr.push('sites=1') 
                    if @$('.share-site-sina:checked').length is 1 
                        sitesArr.push('sites=2') 
                    @model.url = url + sitesArr.join('&')
                    if sitesArr.length is 0
                        @flashMsg.local('请勾选分享站点.')
                    else
                        data = {
                            access_token: window.localStorage.getItem('accessToken'),
                            share: @$('.share-input').val() + ' 看这里' + shareLink
                        }
                        @model.fetch({
                            data: data,
                            wait: true,
                            success: @successFetch, error: @errorFetch
                        })
                else
                    @flashMsg.local('这件物品正在审核.')

        successFetch: (model, response, options)->
            @close()

        errorFetch: (model, response, options)->
            @flashMsg.remote(response)

        showAuth: (event)->
            @$(event.target).find('a').removeClass('hide').addClass('show-inline')

        hideAuth: (event)->
            @$(event.target).find('a').removeClass('show-inline').addClass('hide')
