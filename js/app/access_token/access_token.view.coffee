define [
    'cs!app/lib/composite.view', 
    'underscore',
    'cs!app/csrf/csrf',
    'cs!app/lib/flash_message'
], (CompositeView, _, CSRF, FlashMessage)->

    class AccessTokenView extends CompositeView
    
        className: 'access-token'

        initialize: (options)->
            @code = options.code
            @site = options.site 
            _.bindAll(
                @, 'successToken', 'errorToken', 'successCSRF', 'errorCSRF'
            )
            @flashMessage = new FlashMessage()

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            @token(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMessage.remote(response)

        token: (csrfToken)->
            @model.url = '/api/access_tokens/mine'
            @model.save(
                {
                    csrf_token: csrfToken,
                    access_token: window.localStorage.getItem('accessToken'),
                    code: @code,
                    site: @convertSite(@site)
                }, 
                {wait: true, success: @successToken, error: @errorToken}
            )

        successToken: (model, response, options)->
            @flashMessage.local('已授权.')
            window.App.router.navigate('', {trigger: true})

        errorToken: (model, response, options)->
            @flashMessage.remote(response)

        convertSite: (site)->
            if site is 'douban'
                return '1'
            if site is 'sina'
                return '2'
