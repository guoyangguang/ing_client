define ['backbone', 'cs!app/item/pubitem_her'], (Backbone, PubItemHer)->


    class PubItemsHer extends Backbone.Collection

        initialize: (models, options)->
            @page = 1
            @currentLen = 0
            # number *th
            @number = 0

        model: PubItemHer

        url: "/api/pubitems/her"
