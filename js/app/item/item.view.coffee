define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/template/share/appdata',
    'text!app/template/item/item.html',
    'text!app/template/item/itemimg.html',
], (
    _, CompositeView, AppData, itemHtml, itemimgHtml
)->

 
    class ItemView extends CompositeView

        className: 'item'

        initialize: (options)->
            @item = options.item
            @itemimgs = options.itemimgs
            @currentSelected = 1 
            @total = 1 

        events: {
            'click .item-next': 'selectNext'
        }

        selectNext: ->
            @$(
                "#item-li-#{@currentSelected}"
            ).removeClass('show').addClass('hide')
            @currentSelected = @currentSelected + 1
            @currentSelected = 1 if @currentSelected > @total 
            @$(
                "#item-li-#{@currentSelected}"
            ).removeClass('hide').addClass('show')

        render: ->
            template = _.template(itemHtml)
            @$el.append(template(@item))
            link = @item.link
            if link
                @$('.item-name').after(
                    "<p><a href='#{link}' target='_blank'>参照链接</a></p>"
                )
            if @item.if_bargain
                @$('#item-li-1 div:nth-child(2) p:nth-child(6)').after(
                    "<p>可以议价</p>"
                )
            else
                @$('#item-li-1 div:nth-child(2) p:nth-child(6)').after(
                    "<p>暂不议价</p>"
                )
            state = AppData.state["#{@item.state}"]
            city = AppData.city["#{@item.state}"]["#{@item.city}"]
            @$('#item-li-1 div:nth-child(2) p:nth-child(7)').after(
                "<p><span>地址:</span> #{state}#{city}</p>"
            )
            for itemimg in @itemimgs
                @total++
                @$('ul').append(
                    _.template(itemimgHtml)(itemimg)
                )
                @$("ul li:nth-child(#{@total})").attr(
                    'id', "item-li-#{@total}"
                )
                if itemimg.is_note
                    @$("ul li:nth-child(#{@total}) div:nth-child(1)").after(
                        "<div><p>是购物凭证</p></div>"
                    )
                else
                    @$("ul li:nth-child(#{@total}) div:nth-child(1)").after(
                        "<div><p>不是购物凭证</p></div>"
                    )

            if @total > 1
                @$('ul').after(
                    "<a class='item-next'>下一张</a>"
                )
            @
