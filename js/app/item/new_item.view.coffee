define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/lib/flash_message',
    'cs!app/csrf/csrf',
    'cs!app/item_img/myitem_itemimgs',
    'cs!app/item_img/myitem_itemimgs.view',
    'cs!app/template/share/appdata',
    'text!app/template/item/new.html',
    'text!app/template/share/select_item_category.html',
], (_, CompositeView, FlashMessage, CSRF, MyitemItemimgs, MyitemItemimgsView,
    AppData, newHtml, selectItemCategoryHtml
)-> 

    class NewitemView extends CompositeView

        id: 'new-item'

        initialize: (options)->  
            @$el.html(newHtml)
            @$('.new-item-category').html(selectItemCategoryHtml)
            @genState() 
            @flashMsg = new FlashMessage()
            _.bindAll(
                @, 'successCreate', 'errorCreate', 'successCSRF', 'errorCSRF'
            )

        events: {
            'click .new-item-close': 'close',
            'click .new-item-sub': 'fetchCSRF',
            'change .new-item-state': 'genCity'
        }

        close: ->
            @leave()

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            @create(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMsg.remote(response)

        create: (csrfToken)->
            formData = new FormData(@$('form')[0])
            formData.append(
                'access_token',
                 window.localStorage.getItem('accessToken')
            )
            formData.append('csrf_token', csrfToken)
            # options to tell jQuery not to process data
            options = {
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                wait: true, 
                success: @successCreate,
                error: @errorCreate
            }
            @collection.create(null, options)

        successCreate: (model, response, options)->
            # create and update itemimgs for the new created item
            myitemItemimgs = new MyitemItemimgs(null, {itemId: model.id})
            myitemItemimgsView = new MyitemItemimgsView({collection: myitemItemimgs})
            @parent.appendChild(myitemItemimgsView)
            @leave()
            @flashMsg.local('您的物品将会被审核, 请继续添加更多照片.')

        errorCreate: (model, response, options)->
            @flashMsg.remote(response)

        genState: ->
            selectState = @$('.new-item-state')
            for key, val of AppData.state
                option = "<option value='#{key}'>#{val}</option>"
                selectState.append(option)

        genCity: ->
            state = @$('.new-item-state').val()
            selectCity = @$('.new-item-city').empty()
            for key, val of AppData.city[state]
                option = "<option value='#{key}'>#{val}</option>"
                selectCity.append(option)
