define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/item/item.view',
    'cs!app/like/like',
    'cs!app/like/like.view',
    'cs!app/access_token/share_btn.view',
    'cs!app/comment/comment',
    'cs!app/comment/comments',
    'cs!app/comment/comments.view',
    'cs!app/lib/flash_message',
    'text!app/template/item/myitem_show.html'
], (_, CompositeView, ItemView, Like, LikeView, ShareBtnView,
    Comment, Comments, CommentsView, FlashMessage, myitemShowHtml
)->

 
    class MyItemShowView extends CompositeView

        id: 'myitem-show'

        initialize: (options)->
            @$el.html(myitemShowHtml)
            _.bindAll(@, 'successFetch', 'errorFetch')
            @flashMsg = new FlashMessage()

        events: {
            'click .myitem-show-close': 'close' 
        }

        close: ->
            @leave()

        fetch: (itemID)->
            @model.url = "/api/items/mine/#{itemID}"
            data = {access_token: window.localStorage.getItem('accessToken')}
            @model.fetch(
                {wait: true, data: data, success: @successFetch, error: @errorFetch}
            ) 

        successFetch: (model, response, options)->
            likeView = new LikeView({
                model: new Like(),
                isLiked: @model.get('is_liked'),
                itemId: @model.id,
                likeCount: @model.get('like_count')
            })
            @appendChildTo(likeView, '.myitem-show-header')

            shareBtnView = new ShareBtnView(
                {itemStatus: @model.get('status'), itemId: @model.get('id')}
            )
            @appendChildTo(shareBtnView, '.myitem-show-header')

            itemView = new ItemView({
                item: {
                    detail_img: @model.get('detail_img'),
                    name: @model.get('name'),
                    buy_at: @model.get('buy_at'),
                    percent_new: @model.get('percent_new'),
                    buy_price: @model.get('buy_price'),
                    price: @model.get('price'),
                    express_price: @model.get('express_price'),
                    if_bargain: @model.get('if_bargain'),
                    state: @model.get('state'),
                    city: @model.get('city'),
                    created_at: @model.get('created_at'),
                    description: @model.get('description')
                },
                itemimgs: @model.get('itemimgs')
            })
            @appendChildTo(itemView, '.myitem-show')

            commentList = []
            for comment in @model.get('comments')
                commentList.push(new Comment(comment))
            comments = new Comments(commentList, {itemId: @model.id})
            commentsView = new CommentsView({collection: comments}) 
            @appendChildTo(commentsView, '.myitem-show')

        errorFetch: (model, response, options)->
            @flashMsg.remote(response)
