define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/item/myitem.view',
    'cs!app/item/new_item.view',
    'cs!app/lib/flash_message',
    'cs!app/lib/utils',
    'text!app/template/item/myitems.html',
], (
    _, CompositeView, MyitemView, NewitemView,
    FlashMessage, Utils, myitemsHtml
)-> 


    class MyitemsView extends CompositeView

        id: 'myitems'

        initialize: (options)->  
            @$el.html(myitemsHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(
                @, 'successFetch', 'errorFetch'
            )
            @listenTo(@collection, 'add', @addCallback)
            itemWidth = 235 + 15
            @positions = {
                1: {left: 25, top: 265, bottom: 265},
                2: {left: 25 + itemWidth, top: 265, bottom: 265},
                3: {left: 25 + itemWidth * 2, top: 265, bottom: 265},
                4: {left: 25 + itemWidth * 3, top: 265, bottom: 265}
            }

        events: {
            'click #myitems-publish': 'newItem',
            'click .myitems-more': 'fetch'
        }

        fetch: ->
            data = {
                access_token: window.localStorage.getItem('accessToken'),
                page: @collection.page 
            }
            @collection.fetch({
                data: data,
                wait: true,
                remove: false,
                success: @successFetch, error: @errorFetch
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @, 'myitems-more')

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        newItem: ->
            # newitemView shares collection
            newitemView = new NewitemView({collection: @collection})
            @appendChild(newitemView)

        addCallback: (item)->
            myitemView = new MyitemView({model: item})
            @appendChildTo(myitemView, '.myitems')

            @collection.number++
            thumbHeight = item.get('img_meta').vdimensions.thumb[1]
            myitemView.$('.myitem-thumb').css('height', thumbHeight)
            lefttop = Utils.calculate_position(
                @positions,
                @collection.number,
                myitemView.$el.outerHeight() + 25 
            )
            myitemView.$el.css({
                'position': 'absolute',
                'z-index': 1,
                'left': "#{lefttop[0]}px",
                'top': "#{lefttop[1]}px",
                'width': '235px',
                'border-radius': '5px',
                'background-color': 'rgb(255, 255, 255)',
                'box-shadow': '0 1px 2px 0 rgba(0,0,0,0.22)'
            })

        # fetchMore: (event)->
        #     event.stopPropagation()
        #     and window.App.router.currentView is groupsView
        #     console.log(
        #         $(window).scrollTop() + $(window).height(),
        #         $(document).height() - 100
        #     )
        #     if $(window).scrollTop() + $(window).height() > $(document).height() - 100
        #         # alert('fetchMore start') 
        #         @fetch()
