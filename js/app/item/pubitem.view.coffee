define [
    'cs!app/lib/composite.view',
    'text!app/template/item/pubitem.html'
], (
    CompositeView, pubitemHtml
)->

 
    class PubItemView extends CompositeView

        className: 'pubitem'

        events: {
            'click .pubitem-thumb': 'navPubitemShow'
        }

        navPubitemShow: ->
            window.App.router.pubitem_show(@model.id, @parent)

        render: ->
            template = _.template(pubitemHtml)
            @$el.html(template(@model.attributes))
            @
