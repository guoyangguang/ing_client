define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/item/item.view',
    'cs!app/profile/profile'
    'cs!app/profile/show.view',
    'cs!app/followship/followship',
    'cs!app/followship/followship.view',
    'cs!app/like/like',
    'cs!app/like/like.view',
    'cs!app/access_token/share_btn.view',
    'cs!app/comment/comment',
    'cs!app/comment/comments',
    'cs!app/comment/comments.view',
    'cs!app/lib/flash_message',
    'text!app/template/item/pubitem_show.html'
], (_, CompositeView, ItemView, Profile, ProfileShowView,
    Followship, FollowshipView, Like, LikeView, ShareBtnView,
    Comment, Comments, CommentsView, FlashMessage, pubitemShowHtml
)->

 
    class PubItemShowView extends CompositeView

        id: 'pubitem-show'

        initialize: (options)->
            @$el.html(pubitemShowHtml)
            _.bindAll(@, 'successFetch', 'errorFetch')
            @flashMsg = new FlashMessage()

        events: {
            'click .pubitem-show-close': 'close' 
        }

        close: ->
            @leave()

        fetch: (itemId)->
            @model.url = "/api/pubitems/#{itemId}" 
            data = {access_token: window.localStorage.getItem('accessToken')}
            @model.fetch(
                {wait: true, data: data, success: @successFetch, error: @errorFetch}
            ) 

        successFetch: (model, response, options)->
            likeView = new LikeView({
                model: new Like(),
                isLiked: @model.get('is_liked'),
                itemId: @model.id,
                likeCount: @model.get('like_count')
            })
            @likeView = likeView
            @appendChildTo(likeView, '.pubitem-show-header')

            shareBtnView = new ShareBtnView(
                {itemStatus: @model.get('status'), itemId: @model.get('id')}
            )
            @shareBtnView = shareBtnView
            @appendChildTo(shareBtnView, '.pubitem-show-header')

            itemView = new ItemView({
                item: {
                    detail_img: @model.get('detail_img'),
                    name: @model.get('name'),
                    buy_at: @model.get('buy_at'),
                    percent_new: @model.get('percent_new'),
                    buy_price: @model.get('buy_price'),
                    price: @model.get('price'),
                    express_price: @model.get('express_price'),
                    if_bargain: @model.get('if_bargain'),
                    state: @model.get('state'),
                    city: @model.get('city'),
                    created_at: @model.get('created_at'),
                    description: @model.get('description')
                },
                itemimgs: @model.get('itemimgs')
            })
            @itemView = itemView
            @appendChildTo(itemView, '.pubitem-show')

            commentList = []
            for comment in @model.get('comments')
                commentList.push(new Comment(comment))
            comments = new Comments(commentList, {itemId: @model.id})
            commentsView = new CommentsView({collection: comments}) 
            @commentsView = commentsView
            @appendChildTo(commentsView, '.pubitem-show')

            profile = new Profile(@model.get('profile'))
            profileShowView = new ProfileShowView({model: profile})
            @appendChildTo(profileShowView, '.pubitem-show-profile')

            followshipView = new FollowshipView({
                model: new Followship(),
                isFollowed: @model.get('is_followed'),
                profileId: profile.id
            })
            profileShowView.appendChild(followshipView)

            window.App.router.pubitems_her(profile.id, @)

        errorFetch: (model, response, options)->
            @flashMsg.remote(response)
