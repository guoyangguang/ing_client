define ['backbone', 'cs!app/item/pubitem'], (Backbone, PubItem)->


    class PubItems extends Backbone.Collection

        initialize: (models, options)->
            @page = 1
            @currentLen = 0
            # number *th
            @number = 0

        model: PubItem

        url: "/api/pubitems"

        # sortByPrice: (item)->
        #     item.get('price')
