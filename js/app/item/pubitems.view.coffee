define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/lib/utils',
    'cs!app/item/pubitem.view',
    'cs!app/lib/flash_message',
    'text!app/template/item/pubitems.html'
], (
    _, CompositeView, Utils, PubItemView, FlashMessage, pubitemsHtml
)-> 


    class PubItemsView extends CompositeView

        id: 'pubitems'

        initialize: (options)->  
            @$el.html(pubitemsHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(
                @, 'successFetch', 'errorFetch'
            )
            @listenTo(@collection, 'add', @addCallback)
            itemWidth = 235 + 15
            @positions = {
                1: {left: 25, top: 65, bottom: 65},
                2: {left: 25 + itemWidth, top: 65, bottom: 65},
                3: {left: 25 + itemWidth * 2, top: 65, bottom: 65},
                4: {left: 25 + itemWidth * 3, top: 65, bottom: 65}
            }

        events: {
            'click .pubitems-more': 'fetch'
        }

        fetch: ->
            data = {
                access_token: window.localStorage.getItem('accessToken'),
                page: @collection.page 
            }
            @collection.fetch({
                data: data,
                wait: true,
                remove: false,
                success: @successFetch, error: @errorFetch
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @, 'pubitems-more')

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        addCallback: (pubItem)->
            pubitemView = new PubItemView({model: pubItem})
            @appendChildTo(pubitemView, '.pubitems')

            @collection.number++
            thumbHeight = pubItem.get('img_meta').vdimensions.thumb[1]
            pubitemView.$('.pubitem-thumb').css('height', thumbHeight)
            lefttop = Utils.calculate_position(
                @positions,
                @collection.number,
                pubitemView.$el.outerHeight(true) + 25 
            )
            pubitemView.$el.css({
                'position': 'absolute',
                'z-index': '1',
                'left': "#{lefttop[0]}px",
                'top': "#{lefttop[1]}px",
                'width': '235px',
                'border-radius': '5px',
                'background-color': 'rgb(255, 255, 255)',
                'box-shadow': '0 1px 2px 0 rgba(0,0,0,0.22)'
            })
            # pubitemView.$el.addClass('pure-u-1-2 pure-u-md-1-4 pure-u-lg-1-8')

        # sortBy: ->
        #     # the original order is the response data order
        #     # TODO the collection is sorted, but view not
        #     @collection.comparator = @collection.sortByPrice
        #     @collection.sort()
        #     alert(@collection.pluck('price'))
        #     # for child in @children._wrapped
        #     #     child.leave()
        #     # for item in @collection
        #     #     itemView = new ItemView({model: item})
        #     #     @appendChildTo(itemView, '.items')
