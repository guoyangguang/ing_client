define ['backbone', 'cs!app/item/myitem'], (Backbone, Myitem)->


    class Myitems extends Backbone.Collection

        initialize: (models, options)->
            @page = 1
            @currentLen = 0
            @number = 0

        model: Myitem

        url: '/api/items/mine'
