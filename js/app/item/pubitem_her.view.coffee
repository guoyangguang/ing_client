define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/lib/flash_message',
    'cs!app/item/item.view',
    'cs!app/like/like',
    'cs!app/like/like.view',
    'cs!app/access_token/share_btn.view',
    'cs!app/comment/comment',
    'cs!app/comment/comments',
    'cs!app/comment/comments.view',
    'text!app/template/item/pubitem_her.html'
], (_, CompositeView, FlashMessage, ItemView, Like, LikeView, ShareBtnView,
    Comment, Comments, CommentsView, pubitemHerHtml
)->

 
    class PubItemHerView extends CompositeView

        className: 'pubitem-her'

        initialize: (options)->
            _.bindAll(@, 'successFetch', 'errorFetch')
            @flashMsg = new FlashMessage()

        events: {
            'click .pubitem-her-thumb': 'fetch'
        }

        fetch: ->
            if window.App.router.ifSignin()
                data = {
                    access_token: window.localStorage.getItem('accessToken'),
                    profile_id: @parent.profileId
                }
                @model.fetch({
                    wait: true, data: data, success: @successFetch,
                    error: @errorFetch
                }) 

        successFetch: (model, response, options)->
            likeView = new LikeView({
                model: new Like(),
                isLiked: @model.get('is_liked'),
                itemId: @model.id,
                likeCount: @model.get('like_count')
            })
            @parent.parent.likeView.leave()
            @parent.parent.likeView = likeView
            @parent.parent.appendChildTo(likeView, '.pubitem-show-header')

            shareBtnView = new ShareBtnView(
                {itemStatus: @model.get('status'), itemId: @model.get('id')}
            )
            @parent.parent.shareBtnView.leave()
            @parent.parent.shareBtnView = shareBtnView
            @parent.parent.appendChildTo(shareBtnView, '.pubitem-show-header')

            itemView = new ItemView({
                item: {
                    detail_img: @model.get('detail_img'),
                    name: @model.get('name'),
                    buy_at: @model.get('buy_at'),
                    percent_new: @model.get('percent_new'),
                    buy_price: @model.get('buy_price'),
                    price: @model.get('price'),
                    express_price: @model.get('express_price'),
                    if_bargain: @model.get('if_bargain'),
                    state: @model.get('state'),
                    city: @model.get('city'),
                    created_at: @model.get('created_at'),
                    description: @model.get('description')
                },
                itemimgs: @model.get('itemimgs')
            })
            @parent.parent.itemView.leave()
            @parent.parent.itemView = itemView
            @parent.parent.appendChildTo(itemView, '.pubitem-show')

            commentList = []
            for comment in @model.get('comments')
                commentList.push(new Comment(comment))
            comments = new Comments(commentList, {itemId: @model.id})
            commentsView = new CommentsView({collection: comments}) 
            @parent.parent.commentsView.leave()
            @parent.parent.commentsView = commentsView
            @parent.parent.appendChildTo(commentsView, '.pubitem-show')

        errorFetch: (model, response, options)->
            @flashMsg.remote(response)

        render: ->
            template = _.template(pubitemHerHtml)
            @$el.html(template(@model.attributes))
            @
