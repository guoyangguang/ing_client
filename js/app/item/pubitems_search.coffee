define ['backbone', 'cs!app/item/pubitem'], (Backbone, PubItem)->


    class PubItemsSearch extends Backbone.Collection

        initialize: (models, options)->
            @page = 1
            @currentLen = 0
            # number *th
            @number = 0

        model: PubItem

        url: "/api/pubitems/search_in_city"
