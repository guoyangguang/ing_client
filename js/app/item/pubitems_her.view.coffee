define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/lib/utils',
    'cs!app/item/pubitem_her.view',
    'cs!app/lib/flash_message',
    'text!app/template/item/pubitems_her.html'
], (
    _, CompositeView, Utils, PubItemHerView, FlashMessage, pubitemsHerHtml
)-> 


    class PubItemsHerView extends CompositeView

        id: 'pubitems-her'

        initialize: (options)->  
            @$el.html(pubitemsHerHtml)
            @profileId = options.profileId
            @flashMsg = new FlashMessage()
            _.bindAll(
                @, 'successFetch', 'errorFetch'
            )
            @listenTo(@collection, 'add', @addCallback)
            itemWidth = 235 + 15
            @positions = {
                1: {left: 45, top: 775, bottom: 775},
                2: {left: 45 + itemWidth, top: 775, bottom: 775},
                3: {left: 45 + itemWidth * 2, top: 775, bottom: 775},
                4: {left: 45 + itemWidth * 3, top: 775, bottom: 775}
            }

        events: {
            'click .pubitems-her-more': 'fetch'
        }

        fetch: ->
            if window.App.router.ifSignin()
                data = {
                    access_token: window.localStorage.getItem('accessToken'),
                    profile_id: @profileId,
                    page: @collection.page 
                }
                @collection.fetch({
                    data: data,
                    wait: true,
                    remove: false,
                    success: @successFetch, error: @errorFetch
                })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(@collection, @, 'pubitems-her-more')

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        addCallback: (item)->
            pubitemHerView = new PubItemHerView({model: item})
            @appendChildTo(pubitemHerView, '.pubitems-her')

            @collection.number++
            thumbHeight = item.get('img_meta').vdimensions.thumb[1]
            pubitemHerView.$('.pubitem-her-thumb').css('height', thumbHeight)
            lefttop = Utils.calculate_position(
                @positions,
                @collection.number,
                pubitemHerView.$el.outerHeight(true) + 25 
            )
            pubitemHerView.$el.css({
                'position': 'absolute',
                'left': "#{lefttop[0]}px",
                'top': "#{lefttop[1]}px",
                'z-index': 4,
                'width': '235px',
                'border-radius': '5px',
                'background-color': 'rgb(255, 255, 255)',
                'box-shadow': '0 1px 2px 0 rgba(0,0,0,0.22)'
            })
