define [
    'cs!app/lib/composite.view',
    'text!app/template/item/myitem.html'
], (CompositeView,  myitemHtml)->

 
    class MyitemView extends CompositeView

        className: 'myitem'

        events: {
            'click .myitem-thumb': 'navMyitemShow' 
        }

        navMyitemShow: ->
            window.App.router.myitem_show(@model.id, @parent)

        render: ->
            template = _.template(myitemHtml)
            @$el.html(template(@model.attributes))
            @
