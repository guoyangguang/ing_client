define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/lib/utils',
    'cs!app/item/pubitem.view',
    'cs!app/lib/flash_message',
    'text!app/template/item/pubitems_kind.html'
], (
    _, CompositeView, Utils, PubItemView, FlashMessage, pubitemsKindHtml
)-> 


    class PubItemsKindView extends CompositeView

        id: 'pubitems-kind'

        initialize: (options)->  
            @kind = options.kind
            @city = options.city
            @$el.html(pubitemsKindHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(
                @, 'successFetch', 'errorFetch'
            )
            @listenTo(@collection, 'add', @addCallback)
            itemWidth = 235 + 15
            @positions = {
                1: {left: 25, top: 65, bottom: 65},
                2: {left: 25 + itemWidth, top: 65, bottom: 65},
                3: {left: 25 + itemWidth * 2, top: 65, bottom: 65},
                4: {left: 25 + itemWidth * 3, top: 65, bottom: 65}
            }

        events: {
            'click .pubitems-kind-more': 'fetch'
        }

        fetch: ->
            data = {
                access_token: window.localStorage.getItem('accessToken'),
                kind: @kind,
                page: @collection.page 
            }
            data.city = @city if @city != 'all'
            @collection.fetch({
                data: data,
                wait: true,
                remove: false,
                success: @successFetch, error: @errorFetch
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @, 'pubitems-kind-more')

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        addCallback: (pubItem)->
            pubitemView = new PubItemView({model: pubItem})
            @appendChildTo(pubitemView, '.pubitems-kind')

            @collection.number++
            thumbHeight = pubItem.get('img_meta').vdimensions.thumb[1]
            pubitemView.$('.pubitem-thumb').css('height', thumbHeight)
            lefttop = Utils.calculate_position(
                @positions,
                @collection.number,
                pubitemView.$el.outerHeight(true) + 25 
            )
            pubitemView.$el.css({
                'position': 'absolute',
                'z-index': '1',
                'left': "#{lefttop[0]}px",
                'top': "#{lefttop[1]}px",
                'width': '235px',
                'border-radius': '5px',
                'background-color': 'rgb(255, 255, 255)',
                'box-shadow': '0 1px 2px 0 rgba(0,0,0,0.22)'
            })
