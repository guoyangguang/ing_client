define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/csrf/csrf',
    'cs!app/lib/flash_message',
    'text!app/template/like/like.html'
], (_, CompositeView, CSRF, FlashMesssage, likeHtml)->


    class LikeView extends CompositeView

        className: 'like'

        initialize: (options)->
            @$el.html(likeHtml)
            @isLiked = options.isLiked
            @itemId = options.itemId
            @$('.like-count').text(options.likeCount)
            _.bindAll(
                @, 'successCSRF', 'errorCSRF', 'successLike',
                'errorLike', 'successUnLike', 'errorUnLike' 
            )
            @flashMsg = new FlashMesssage()

        events: {
            'click .like-unlike': 'fetchCSRF'
        }

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            if @isLiked
                @unlike(model.get('csrf_token'))
            else
                @like(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMsg.remote(response)

        like: (csrfToken)->
            if window.App.router.ifSignin()
                @model.url = '/api/likes/mine'
                data = {
                    csrf_token: csrfToken,
                    access_token: window.localStorage.getItem('accessToken'),
                    id: @itemId
                }
                @model.save(
                    data,
                    {type: 'POST', wait: true, success: @successLike, error: @errorLike}
                )

        successLike: (model, response, options)->
            # TODO change like image
            @$('.like-count').text(model.get('item_liked_count'))
            @isLiked = true
            @flashMsg.local('已喜欢.')

        errorLike: (model, response, options)->
            @flashMsg.remote(response)

        unlike: (csrfToken)->
            if window.App.router.ifSignin()
                @model.url = "/api/likes/mine/#{@itemId}"
                data = {
                    csrf_token: csrfToken,
                    access_token: window.localStorage.getItem('accessToken')
                }
                @model.save(
                    data,
                    {type: 'PUT', wait: true, success: @successUnLike, error: @errorUnLike}
                )

        successUnLike: (model, response, options)->
            # TODO change like image
            @$('.like-count').text(model.get('item_liked_count'))
            @isLiked = false 
            @flashMsg.local('已取消喜欢.')

        errorUnLike: (model, response, options)->
            @flashMsg.remote(response)
