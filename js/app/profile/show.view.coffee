define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/message/new'
    'cs!app/message/new.view'
    'text!app/template/profile/show.html'
], (_, CompositeView, NewMsg, NewMsgView, showHtml)->


    class ProfileShowView extends CompositeView

        id: 'profile-show'

        initialize: (options)->
            @hasNewMsg = false

        events: {
            'click .profile-show-msg': 'newMsg'
        }

        newMsg: (event)->
            if not @hasNewMsg
                newMsg = new NewMsg()
                newMsgView = new NewMsgView({'model': newMsg, 'theOther': @model.id})
                @appendChild(newMsgView)
                newMsgView.$el.css({
                    'position': 'absolute',
                    'z-index': 5,
                    'right': "325px",
                    'top': "385px",
                    'width': '325px',
                    'height': '215px',
                    'padding': '15px',
                    'border-radius': '5px',
                    'background-color': 'rgb(255, 255, 255)',
                    'box-shadow': '0 1px 2px 0 rgba(0,0,0,0.22)'
                })
                @hasNewMsg = true

        render: ->
            template = _.template(showHtml)
            if @model.get('gender') is 0
               @model.set('gender', '男')
            else
               @model.set('gender', '女')
            @$el.html(template(@model.attributes))
            @
