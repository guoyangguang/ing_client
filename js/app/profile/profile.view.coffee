define [
   'underscore',
   'cs!app/lib/composite.view',
   'cs!app/csrf/csrf',
   'cs!app/lib/flash_message',
   'text!app/template/profile/profile.html',
   'text!app/template/profile/edit.html',
   'text!app/template/profile/upload.html'
], (_, CompositeView, CSRF, FlashMessage, profileHtml, editHtml, uploadHtml)->


    class ProfileView extends CompositeView

        id: 'profile'
 
        initialize: (options)->
            @$el.html(profileHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(
                @, 'successFetch', 'errorFetch', 'successCSRF', 'errorCSRF',
                'successUpload', 'errorUpload', 'successUpdate', 'errorUpdate'
            )
            @listenTo(@model, 'change', @changeCallback)

        events: {
            'change #profile-upload': 'fetchCSRF',
            'click #profile-save': 'fetchCSRF'
        }

        fetch: ->
            accessToken = window.localStorage.getItem('accessToken')
            @model.url = "/api/profile?access_token=#{accessToken}"
            @model.fetch({
                wait: true,
                success: @successFetch,
                error: @errorFetch
            })
       
        successFetch: (model, response, options)->

        errorFetch: (model, response, options)->
            @flashMsg.remote(response)

        fetchCSRF: (event)->
            if event.target.id is 'profile-upload'
                @csrfFor = @upload
            if event.target.id is 'profile-save'
                @csrfFor = @update
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            @csrfFor(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMsg.remote(response)

        update: (csrfToken)->
            data = {
                'access_token': window.localStorage.getItem('accessToken'),
                'csrf_token': csrfToken,
                'name': @$("input[name='profile_name']").val(),
                'gender': @$("input[name='profile_gender']:checked").val(),
                'location': @$("input[name='profile_location']").val(),
                'job': @$("select[name='profile_job']").val(),
                'about': @$("textarea[name='profile_about']").val(),
            }
            @model.url = "/api/profile"
            @model.save(
                data,
                {wait: true, success: @successUpdate, error: @errorUpdate}
            )
       
        successUpdate: (model, response, options)->
            @flashMsg.local('已更新!')

        errorUpdate: (model, response, options)->
            @flashMsg.remote(response)

        upload: (csrfToken)->
            formData = new FormData(@$('form')[0])
            accessToken = window.localStorage.getItem('accessToken')
            formData.append('access_token', accessToken)
            formData.append('csrf_token', csrfToken)
            @model.url = '/api/profile/file'
            # options to tell jQuery not to process data
            options = {
                type: 'PUT',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                wait: true, 
                success: @successUpload,
                error: @errorUpload
            }
            @model.save(null, options)
        
        successUpload: (model, response, options)->
            @flashMsg.local('已上传!')

        errorUpload: (model, response, options)->
            @flashMsg.remote(response)

        changeCallback: ->
            if @model.hasChanged('name') or @model.hasChanged('gender') or
            @model.hasChanged('location') or @model.hasChanged('job') or
            @model.hasChanged('about')
                @$('#profile-general').html(
                    _.template(editHtml)(@model.attributes)
                )
                if @model.get('gender') == 0
                    @$("input[value='0']").attr('checked': 'checked')
                else
                    @$('input[value="1"]').attr('checked': 'checked')
                job = @model.get('job')
                if job
                    @$("select[name='profile_job'] option[value='#{job}']").attr(
                        'selected': 'selected'
                    )
                else
                    @$("select[name='profile_job'] option[value='1']").attr(
                        'selected': 'selected'
                    )
            if @model.hasChanged('detail_file')
                uploadForm = @$('#profile-upload-form')
                if not @$('#profile-upload').length
                    uploadForm.html(uploadHtml)
                img = "<img id='profile-img' src='<%= detail_file %>'>"
                imgWithData = _.template(img)({'detail_file': @model.get('detail_file')})
                profileImg = uploadForm.find('#profile-img')
                if profileImg.length
                    profileImg.replaceWith(imgWithData)
                else
                    uploadForm.prepend(imgWithData)
