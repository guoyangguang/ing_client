define [
  'cs!app/lib/composite.view',
  'cs!app/template/share/appdata'
], (CompositeView, AppData) ->

  
    class LocationView extends CompositeView 
    
        tagName: 'ul'
        id: 'location' 

        initialize: (options)->
            @$el.append("<li class='location-all'>所有</li>")
            for stateRepre, state of AppData.state
                @$el.append("<li data-state='#{stateRepre}'>#{state}</li>")
                cities = ''
                for cityRepre, city of AppData.city[stateRepre]
                    cities = cities + "<li data-city='#{cityRepre}' class='location-city'>#{city}</li>"
                @$el.append("<li><ul>" + cities + "</ul></li>")
            
        events: {
            'click .location-all': 'selectAllCities',
            'click .location-city': 'selectCity'
        }

        selectAllCities: ->
            @parent.city = null 
            @parent.$('#menu-main-city').html('所有')
            @parent.location = null
            @leave()

        selectCity: (event)->
            @parent.city = event.target.dataset.city
            @parent.$('#menu-main-city').html(event.target.innerText)
            @parent.location = null
            @leave()
