define [
  'cs!app/lib/composite.view',
  'text!app/template/menu/category.html'
], (CompositeView, categoryHtml) ->

  
    class CategoryView extends CompositeView 
    
        tagName: 'ul'
        id: 'category' 

        initialize: (options)->
            @$el.html(categoryHtml)
            
        events: {
            'click li': 'navPubitemsKind'
        }

        navPubitemsKind: (event)->
            @parent.category = null
            @leave()
            city = if @parent.city then @parent.city else 'all' 
            kind = event.target.dataset.cate
            window.App.router.navigate(
                "pubitems/kind/#{kind}/#{city}",
                {trigger: true}
            )
