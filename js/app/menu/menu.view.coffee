define [
  'cs!app/lib/composite.view',
  'cs!app/menu/category.view',
  'cs!app/menu/location.view',
  'text!app/template/menu/menu.html'
], (CompositeView, CategoryView, LocationView, menuHtml) ->

  
    class MenuView extends CompositeView 
    
        tagName: 'nav'
        id: 'menu' 

        initialize: (options)->
            @$el.html(menuHtml)
            
        events: {
            'click #menu-main-name': 'navPubitems',
            'click #menu-main-city': 'appendLocation',
            'click #menu-main-category': 'appendCate',
            'submit #menu-main-items-search': 'navPubitemsSearch',
            'click #menu-main-msg': 'navUnreadMsg',
            'click #menu-main-me': 'navMyItem',
            'click #menu-msg-contacts': 'navContactsMine',
            'click #menu-msg-unread': 'navUnreadMsg',
            'click #menu-me-myitem': 'navMyItem',
            'click #menu-me-mylike': 'navPubitemsMyLike',
            'click #menu-me-profile': 'navProfile',
            'click #menu-me-signout': 'navSignout'
        }

        navPubitems: ->
            window.App.router.navigate('', {trigger: true})

        appendLocation: ->
            if @location
                @location.leave()
                @location = null
            else
                locationView = new LocationView()
                @appendChild(locationView)
                @location = locationView

        appendCate: (event)->
            if @category
                @category.leave()
                @category = null
            else
                categoryView = new CategoryView()
                @appendChild(categoryView)
                @category = categoryView

        navPubitemsSearch: ->
            query = window.App.router.menuView.$('#menu-main-items-search input').val()
            city = if @city then @city else 'all' 
            window.App.router.navigate(
                "pubitems/search/#{query}/#{city}",
                {trigger: true}
            )

        navUnreadMsg: ->
            window.App.router.navigate('messages/unread/mine', {trigger: true})

        navMyItem: ->
            window.App.router.navigate('items/mine', {trigger: true})

        navPubitemsMyLike: ->
            window.App.router.navigate('pubitems/likes/mine', {trigger: true})

        navContactsMine: ->
            window.App.router.navigate('messages/contacts/mine', {trigger: true})

        navProfile: ->
            window.App.router.navigate('profile', {trigger: true})
       
        navSignout: ->
            window.App.router.navigate('signout', {trigger: true})

        swapMenu: (menuSelector=null)->
            @currentSub.removeClass('show').addClass('hide') if @currentSub
            if menuSelector
                @currentSub = @$(menuSelector)
                @currentSub.removeClass('hide').addClass('show')
        
        hide: ->
            @$('#menu-me').removeClass('show').addClass('hide') 
            @$('#menu-msg').removeClass('show').addClass('hide')
