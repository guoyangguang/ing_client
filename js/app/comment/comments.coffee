define [
    'backbone',
    'cs!app/comment/comment'
], (Backbone, Comment)->


    class Comments extends Backbone.Collection

        initialize: (models, options)->
           @itemId = options.itemId if options.itemId

        model: Comment

        url: -> "/api/pubitems/#{@itemId}/comments"
