define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/lib/flash_message',
    'cs!app/comment/reply.view',
    'text!app/template/comment/comment.html',
    'text!app/template/comment/replied.html'
], (_, CompositeView, FlashMessage, ReplyView, commentHtml, repliedHtml)->


    class CommentView extends CompositeView

        tagName: 'li'
        className: 'comment'

        initialize: (options)->

        events: {
            'click .comment-reply': 'reply'
        }

        reply: ->
            if @parent.reply
                @parent.reply.leave()
            # model is shared
            replyView = new ReplyView({model: @model})
            @parent.prependChildTo(replyView, '.comments-create')
            @parent.reply = replyView 

        render: ->
            @$el.html(
                _.template(commentHtml)(@model.attributes)
            )
            comment = @model.get('comment')
            if comment
                @$('.comment-body').prepend(
                    _.template(repliedHtml)(comment)
                )
            @
