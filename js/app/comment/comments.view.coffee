define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/csrf/csrf',
    'cs!app/lib/flash_message',
    'cs!app/comment/comment.view',
    'text!app/template/comment/comments.html'
], (_, CompositeView, CSRF, FlashMessage, CommentView, commentsHtml)->


    class CommentsView extends CompositeView

        id: 'comments'

        initialize: (options)->
            @$el.html(commentsHtml)
            _.bindAll(
                @, 'addCallback', 'successCreate', 'errorCreate',
                'successCSRF', 'errorCSRF'
            )
            @listenTo(@collection, 'add', @addCallback)
            @flashMsg = new FlashMessage()
            @collection.forEach(@addCallback) 
            @reply = null

        events: {
            'click .comments-send': 'fetchCSRF'
        }

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            @create(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMsg.remote(response)

        create: (csrfToken)->
            if window.App.router.ifSignin()
                data = {
                    csrf_token: csrfToken,
                    access_token: window.localStorage.getItem('accessToken'),
                    body: @$('.comments-body').val()
                }
                if @reply
                    commentId = @reply.$('.comment-id').attr('val')                
                    data.comment_id = commentId
                @collection.create(
                    data,
                    {wait: true, success: @successCreate, error: @errorCreate}
                )

        successCreate: (model, response, options)->
            if @reply
                @reply.leave()
                @reply = null
            @$('.comments-body').val('')

        errorCreate: (model, response, options)->
            @flashMsg.remote(response)

        addCallback: (comment)->
            commentView = new CommentView({model: comment})
            @appendChildTo(commentView, '.comments')
