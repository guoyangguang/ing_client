define ['underscore', 'jquery'], (_, $)->
  
    class CountUnreadMsgsWs
    
        constructor: (token)->
            _.bindAll(@, 'onOpen', 'onClose', 'onError', 'onMsg') 
            @token = token 
            @webSocket = new WebSocket("ws://127.0.0.1:8080?token=#{@token}")
            @webSocket.onopen = @onOpen
            @webSocket.onclose = @onClose 
            @webSocket.onerror = @onError 
            @webSocket.onmessage = @onMsg
       
        onOpen: ->
            # when shakehand is successfull and connection is build,
            # it will be triggered
            console.log "Cum webSocket connetction open for #{@token}"

        onClose: ->
            # when connection is closed, it will be triggered 
            unreadTag = $('#menu-msg-unread-count')
            unreadTag.empty()
            console.log "Cum webSocket connection closed for #{@token}"

        onError: -> 
            console.log "Cum webSocket connection errors for #{@token}"

        onMsg: (msg)->
            # when received msgs, it will be triggered
            num = Number(msg.data)
            unreadTag = $('#menu-msg-unread-count')
            if num is 0
                unreadTag.empty()
            else
                unreadTag.text("#{num}")

        close: ->
            @webSocket.close()
