require ['jquery'], ($)->
    $(document).ready ->
        $(document).ajaxStart -> $('#loading').fadeIn()
        $(document).ajaxStop -> $('#loading').fadeOut()

