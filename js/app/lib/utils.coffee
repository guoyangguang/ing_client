define [], ()->

    class Utils

        @ifFetchMore: (collection, view, moreClassName, moreContainer=null)->
            if collection.length isnt 0
                if collection.length is collection.currentLen
                    view.$(".#{moreClassName}").addClass('hide')
                else
                    if collection.page is 1
                        moreLink = "<a class='#{moreClassName}'>更多...</a>"
                        if moreContainer
                            view.$(moreContainer).append(moreLink)
                        else
                            view.$el.append(moreLink)
                    collection.currentLen = collection.length
                    collection.page++

        @calculate_position: (positions, number, height)->
            # :param positions: object containing position for every view 
            # :param number: number *th
            # :param height:, the number *th view height
            if number <= 4
                position = positions[String(number)]
                position.bottom += height
            else
                referPos = positions[String(number - 4)]
                positions[String(number)] = {}
                position = positions[String(number)]
                position.left = referPos.left
                position.top = referPos.bottom
                position.bottom = position.top + height
            return [position.left, position.top]

