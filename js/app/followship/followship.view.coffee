define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/csrf/csrf',
    'cs!app/lib/flash_message'
], (_, CompositeView, CSRF, FlashMesssage)->


    class FollowshipView extends CompositeView

        tagName: 'button'
        className: 'followship pure-button'

        initialize: (options)->
            @isFollowed = options.isFollowed if options.isFollowed
            @profileId = options.profileId if options.profileId
            if @isFollowed
                @$el.text('取消关注')
            else
                @$el.text('关注')
            _.bindAll(
                @, 'successCSRF', 'errorCSRF', 'successFollow',
                'errorFollow', 'successUnFollow', 'errorUnFollow' 
            )
            @flashMsg = new FlashMesssage()

        events: {
            'click': 'fetchCSRF'
        }

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            token = model.get('csrf_token')
            if @isFollowed
                @unfollow(token)
            else
                @follow(token)

        errorCSRF: (model, response, options)->
            @flashMsg.remote(response)

        follow: (csrfToken)->
            if window.App.router.ifSignin()
                @model.url = '/api/followeds/mine'
                data = {
                    csrf_token: csrfToken,
                    access_token: window.localStorage.getItem('accessToken'),
                    id: @profileId
                }
                options = {
                    type: 'POST',
                    wait: true,
                    success: @successFollow,
                    error: @errorFollow
                }
                @model.save(
                    data,
                    options
                )

        successFollow: (model, response, options)->
            @$el.text('取消关注')
            @isFollowed = true
            @flashMsg.local('已关注.')

        errorFollow: (model, response, options)->
            @flashMsg.remote(response)

        unfollow: (csrfToken)->
            if window.App.router.ifSignin()
                @model.url = "/api/followeds/mine/#{@profileId}"
                data = {
                    csrf_token: csrfToken,
                    access_token: window.localStorage.getItem('accessToken')
                }
                options = {
                    type: 'PUT',
                    wait: true,
                    success: @successUnFollow,
                    error: @errorUnFollow
                }
                @model.save(
                    data,
                    options
                )

        successUnFollow: (model, response, options)->
            @$el.text('关注')
            @isFollowed = false 
            @flashMsg.local('已取消关注.')

        errorUnFollow: (model, response, options)->
            @flashMsg.remote(response)
