define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/item_img/myitem_itemimg.view',
    'cs!app/lib/flash_message',
    'cs!app/csrf/csrf',
    'text!app/template/item_img/myitem_itemimgs.html'
], (
    _, CompositeView, MyitemItemimgView,
    FlashMessage, CSRF, MyitemItemimgsHtml
)-> 


    class MyitemItemimgsView extends CompositeView

        id: 'myitem-itemimgs'

        initialize: (options)->  
            @$el.html(MyitemItemimgsHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(
                @, 'successCreate', 'errorCreate', 'successCSRF', 'errorCSRF'
            )
            @listenTo(@collection, 'add', @addCallback)

        events: {
            'change #myitem-itemimgs-file': 'fetchCSRF',
            'click .myitem-itemimgs-close': 'close'
        }

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            @create(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMsg.remote(response)

        create: (csrfToken)->
            formData = new FormData(@$('form')[0])
            formData.append(
                'access_token',
                 window.localStorage.getItem('accessToken')
            )
            formData.append('csrf_token', csrfToken)
            # options to tell jQuery not to process data
            options = {
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                wait: true, 
                success: @successCreate,
                error: @errorCreate
            }
            @collection.create(null, options)

        successCreate: (model, response, options)->
            @flashMsg.local('图片已上传.')

        errorCreate: (model, response, options)->
            @flashMsg.remote(response)

        close: ->
            @leave()

        addCallback: (itemimg)->
            myitemItemimgView = new MyitemItemimgView({model: itemimg})
            @appendChildTo(myitemItemimgView, '.myitem-itemimgs')
