define ['backbone', 'cs!app/item_img/myitem_itemimg'], (Backbone, MyitemItemimg)->


    class MyitemItemimgs extends Backbone.Collection

        initialize: (models, options)->
            @itemId = options.itemId if options.itemId

        model: MyitemItemimg

        url: ->
            "/api/items/mine/#{@itemId}/itemimgs"
