define [
    'underscore',
    'cs!app/lib/composite.view',
    'cs!app/lib/flash_message',
    'cs!app/csrf/csrf',
    'text!app/template/item_img/myitem_itemimg_edit.html',
    'text!app/template/item_img/myitem_itemimg.html'
], (_, CompositeView, FlashMessage, CSRF,
    MyitemItemimgEditHtml, MyitemItemimgHtml
)->

 
    class MyitemItemimgView extends CompositeView

        tagName: 'li'
        className: 'myitem-itemimg pure-form'

        initialize: (options)->
            _.bindAll(@, 'successUpdate', 'errorUpdate', 'successCSRF', 'errorCSRF')
            @listenTo(@model, 'change', @changeCallback)
            @flashMsg = new FlashMessage()

        events: {
            'click .myitem-itemimg-sub': 'fetchCSRF'
        }

        render: ->
            template = _.template(MyitemItemimgEditHtml)
            @$el.html(template(@model.attributes))
            @

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            @update_general(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMsg.remote(response)

        update_general: (csrfToken)->
            data = {
                csrf_token: csrfToken,
                access_token: window.localStorage.getItem('accessToken'),
                is_note: @$("input[name='#{@model.id}_is_note']:checked").val(),
                description: @$("textarea[name='description']").val()
            }
            @model.save(
                data,
                {wait: true, success: @successUpdate, error: @errorUpdate}
            )

        successUpdate: (model, response, options)->
            @flashMsg.local('已更新.')

        errorUpdate: (model, response, options)->
            @flashMsg.remote(response)

        changeCallback: ->
            template = _.template(MyitemItemimgHtml)
            # avoid triggering changeCallback
            _obj = {'description': @model.get('description')}
            if @model.get('is_note')
               _obj['is_note'] = '是'
            else
               _obj['is_note'] = '不是'
            @$('.myitem-itemimg-general').html(
                template(_obj)
            )
