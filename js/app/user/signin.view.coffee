define [
    'cs!app/lib/composite.view',
    'underscore',
    'cs!app/lib/count_unread_msgs_ws',
    'cs!app/csrf/csrf',
    'cs!app/lib/flash_message',
    'text!app/template/user/signin.html'
], (CompositeView, _, CountUnreadMsgsWs, CSRF, FlashMessage, signinHtml)->
  
    class SigninView extends CompositeView
    
        id: 'signin'

        className: 'pure-form'

        initialize: (options)->
            @$el.html(signinHtml)
            _.bindAll(
                @, 'errorCreate', 'successCreate', 'successFetch', 'errorFetch'
            )
            @flashMessage = new FlashMessage()

        events: {
            'click .signin-submit': 'fetchCSRF',
            'click .signin-signup': 'navSignup',
            'click .signin-forget-password': 'navForgetPassword'
        }

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successFetch, error: @errorFetch})

        successFetch: (model, response, options)->
            @signin(model.get('csrf_token'))

        errorFetch: (model, response, options)->
            @flashMessage.remote(response)

        signin: (csrfToken)->
            @model.url = '/api/signin'
            signinData = {
                csrf_token: csrfToken,
                captcha_data: @$('input[name="captcha"]').val(),
                phone: @$('input[name="phone"]').val(),
                password: @$('input[name="password"]').val()
            }
            @model.save(
                signinData,
                {
                    wait: true, 
                    success: @successCreate,
                    error: @errorCreate
                }
            ) 
        
        successCreate: (model, response, options)->
            window.localStorage.setItem('accessToken', @model.attributes.access_token) 
            @model.clear {silent: true}

            window.App.countUnreadMsgsWs = new CountUnreadMsgsWs(
                window.localStorage.getItem('accessToken')
            )
            window.App.router.navigate('', {trigger: true})

        errorCreate: (model, response, options)->                   
            @flashMessage.remote(response)

        navSignup: ->
            window.App.router.navigate('signup', {trigger: true})

        navForgetPassword: ->
            window.App.router.navigate('reset_password', {trigger: true})
