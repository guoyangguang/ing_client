define [
    'cs!app/lib/composite.view', 
    'underscore',
    'cs!app/csrf/csrf',
    'cs!app/lib/flash_message'
], (CompositeView, _, CSRF, FlashMessage)->

    class SignoutView extends CompositeView

        initialize: (options)->
            _.bindAll(
                @, 'successSignout', 'errorSignout', 'successFetch', 'errorFetch' 
            )
            @flashMessage = new FlashMessage()

        fetchCSRF: ->
            csrf = new CSRF()
            csrf.fetch({success: @successFetch, error: @errorFetch})

        successFetch: (model, response, options)->
            @signOut(model.get('csrf_token'))

        errorFetch: (model, response, options)->
            @flashMessage.remote(response)
       
        signOut: (csrfToken)->
            @model.url = '/api/signout' 
            accessToken = window.localStorage.getItem('accessToken')
            @model.save(
              {csrf_token: csrfToken, access_token: accessToken},
              {wait: true, success: @successSignout, error: @errorSignout}
            )

        successSignout: (model, response, options)->
            # window.App.countUnreadMsgsWs.close()
            window.localStorage.removeItem('accessToken')
            window.App.router.navigate('signin', {trigger: true})
        
        errorSignout: (model, response, options)->
            @flashMessage.remote(response) 
