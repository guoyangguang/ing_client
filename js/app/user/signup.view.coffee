define [
    'cs!app/lib/composite.view',
    'cs!app/csrf/csrf',
    'cs!app/verification/verification',
    'cs!app/lib/flash_message',
    'text!app/template/user/signup.html'
], (CompositeView, CSRF, Verification, FlashMessage, signupHtml)->
  
    class SignupView extends CompositeView
    
        id: 'signup'

        className: 'pure-form'

        initialize: (options)->
            @$el.html signupHtml 
            _.bindAll(
                @, 'successCSRF', 'errorCSRF', 'successSignup', 'errorSignup',
                'successVerif', 'errorVerif', 'timer'
            )
            @flashMessage = new FlashMessage()
            @startTime = 60
            @phoneRe = new RegExp("^1[0-9]{10}$")

        events: {
            'click .signup-submit': 'fetchCSRF',
            'click .signup-send-verif': 'fetchCSRF',
            'click .signup-signin': 'navSignin'
        }

        fetchCSRF: ->
            if event.target.id is 'signup-send-verif'
                @csrfFor = @sendVerificationCode
            if event.target.id is 'signup-submit'
                @csrfFor = @signup
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            @csrfFor(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMessage.remote(response)

        signup: (csrfToken)->
            if @$("input[name='accept']:checked").val()
                @model.url = '/api/signup' 
                signupData = {
                    csrf_token: csrfToken,
                    captcha_data: @$('input[name="captcha"]').val(),
                    phone: @$('input[name="phone"]').val(),
                    verification_code: @$('input[name="verification_code"]').val(),
                    email: @$('input[name="email"]').val(),
                    password: @$('input[name="password"]').val(),
                    password_confirmation: @$('input[name="password_confirmation"]').val(),
                    accept: @$("input[name='accept']:checked").val()
                }
                @model.save(
                    signupData,
                    {
                        wait: true, 
                        success: @successSignup,
                        error: @errorSignup
                    }
                ) 
            else
                @flashMessage.local('请阅读并接受协议条款.')
        
        successSignup: (model, response, options)->
            window.App.router.navigate('signin', {trigger: true})
        
        errorSignup: (model, response, options)->
            @flashMessage.remote(response)

        sendVerificationCode: (csrfToken)->
            phone = @$('input[name="phone"]').val()
            if @phoneRe.exec(phone)
                verification = new Verification()
                verification.save(
                    {
                        csrf_token: csrfToken,
                        phone: phone 
                    },
                    {
                        wait: true, 
                        success: @successVerif,
                        error: @errorVerif
                    }
                )
                @$('.signup-send-verif').replaceWith(
                    '<button class="timer pure-button"><span class="timer-sec">60</span>秒后重新获取</button>'
                )
                @interval = window.setInterval(@timer, 1000)
            else
                @flashMessage.local('请输入手机号码.')

        successVerif: (model, response, options)->

        errorVerif: (model, response, options)->
            @flashMessage.remote(response)

        navSignin: ->
            window.App.router.navigate('signin', {trigger: true})

        timer: ->
        # timer let user wait some time to launch next request
            @startTime = @startTime - 1
            if @startTime <= 0
                clearInterval(@interval)
                @$('.timer').replaceWith(
                    "<button id='signup-send-verif' class='signup-send-verif pure-button' type='button'>获取验证码</button>"
                )
                @startTime = 60
                return @startTime
            @$('.timer-sec').html(@startTime)
