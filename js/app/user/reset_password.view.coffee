define [
    'cs!app/lib/composite.view',
    'underscore',
    'cs!app/csrf/csrf',
    'cs!app/verification/verification',
    'cs!app/lib/flash_message',
    'text!app/template/user/reset_password.html'
], (CompositeView, _, CSRF, Verification, FlashMessage, resetPasswordHtml)->
  
    class ResetPasswordView extends CompositeView
    
        id: 'reset-password'

        className: 'pure-form'

        initialize: (options)->
            @$el.html(resetPasswordHtml)
            _.bindAll(
                @, 'successCSRF', 'errorCSRF', 'successReset', 'errorReset',
                'successVerif', 'errorVerif', 'timer' 
            )
            @flashMessage = new FlashMessage()
            @startTime = 60
            @phoneRe = new RegExp("^1[0-9]{10}$")

        events: {
            'click .reset-password-submit': 'fetchCSRF', 
            'click .reset-password-send-verif': 'fetchCSRF' 
        }

        fetchCSRF: (event)->
            if event.target.id is 'reset-password-send-verif'
                @csrfFor = @sendVerificationCode
            if event.target.id is 'reset-password-submit'
                @csrfFor = @resetPassword
            csrf = new CSRF()
            csrf.fetch({success: @successCSRF, error: @errorCSRF})

        successCSRF: (model, response, options)->
            @csrfFor(model.get('csrf_token'))

        errorCSRF: (model, response, options)->
            @flashMsg.remote(response)

        resetPassword: (csrfToken)->
            @model.url = '/api/reset_password'
            data = {
                csrf_token: csrfToken,
                captcha_data: @$('input[name="captcha"]').val(),
                phone: @$('input[name="phone"]').val(),
                verification_code: @$('input[name="verification_code"]').val(),
                password: @$('input[name="password"]').val(), 
                password_confirmation: @$('input[name="password_confirmation"]').val(), 
            }
            @model.save(
                data,
                {wait: true, success: @successReset, error: @errorReset} 
            )

        successReset: (model, response, options)->
            @flashMessage.local('请用新密码登录.') 
            window.App.router.navigate('signin', {trigger: true})

        errorReset: (model, response, options)->
            @flashMessage.remote(response) 

        sendVerificationCode: (csrfToken)->
            phone = @$('input[name="phone"]').val()
            if @phoneRe.exec(phone)
                verification = new Verification()
                verification.save(
                    {
                        csrf_token: csrfToken,
                        phone: phone 
                    },
                    {
                        wait: true, 
                        success: @successVerif,
                        error: @errorVerif
                    }
                )
                @$('.reset-password-send-verif').replaceWith(
                    '<button class="timer pure-button"><span class="timer-sec">60</span>秒后重新获取</button>'
                )
                @interval = window.setInterval(@timer, 1000)
            else
                @flashMessage.local('请输入手机号码.')

        successVerif: (model, response, options)->

        errorVerif: (model, response, options)->
            @flashMessage.remote(response)

        timer: ->
            @startTime = @startTime - 1
            if @startTime <= 0
                clearInterval(@interval)
                @$('.timer').replaceWith(
                    "<button id='reset-password-send-verif' class='reset-password-send-verif pure-button' type='button'>获取验证码</button>"

                )
                @startTime = 60
                return @startTime
            @$('.timer-sec').html(@startTime)
