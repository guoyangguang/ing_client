require.config({
    paths: {
        'coffee-script': "lib/coffee-script",
        'cs': "lib/cs",
        'text': "lib/text",
        'jquery': "lib/jquery",
        'underscore': "lib/underscore",
        'backbone': "lib/backbone"
    },
    shim: {
        'underscore': {exports: "_"}, 
        'backbone': {deps: ["underscore", "jquery"], exports: "Backbone"},
    }
}); 

require(["cs!app"]);
