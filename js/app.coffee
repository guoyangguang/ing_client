require [
    'jquery', 
    'backbone',
    'cs!app/router',
    'cs!app/lib/count_unread_msgs_ws'
], ($, Backbone, Router, CountUnreadMsgsWs) ->

    $(document).ready ->
        window.App = {}
        window.App.router = new Router()
        Backbone.history.start(pushState: true) 
        # UnreadMsgsWs connect
        # if token = window.localStorage.getItem('accessToken')
        #     window.App.countUnreadMsgsWs = new CountUnreadMsgsWs(token)
        $(document).ajaxStart -> $('#loading').fadeIn()
        $(document).ajaxStop -> $('#loading').fadeOut()
