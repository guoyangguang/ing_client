// To understand relative path and other concept,
// visit http://www.requirejs.cn/docs/optimization.html
// https://github.com/fmarcia/UglifyCSS
({
    appDir: ".",
    baseUrl: "js",
    stubModules: ['cs'],
    paths: {
        'coffee-script': "lib/coffee-script",
        'cs': "lib/cs",
        'text': "lib/text",
        'jquery': "lib/jquery",
        'underscore': "lib/underscore",
        'backbone': "lib/backbone"
    },
    shim: {
        'underscore': {exports: "_"}, 
        'backbone': {deps: ["underscore", "jquery"], exports: "Backbone"},
    },
    modules: [
        {name: 'main', exclude: ['coffee-script'], include: ['lib/require']}
    ],
    optimize: "uglify",
    dir: "../build"
})
